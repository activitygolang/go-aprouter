
#
# Makefile for go-aprouter
#

build:
	go build ./router.go
test:
	go test ./...
govet:
	go vet ./...
gofmt goformat:
	go fmt ./...

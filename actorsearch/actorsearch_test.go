package actorsearch

import (
	"testing"
	"reflect"
)

func TestActorFind_hardcode(t *testing.T) {
	// construct test objects
	af := NewActorFinder()
	hardcode := map[string]string{"foo": "alpha"}
	af.SetHardcodedMode(hardcode)
	// startup
	af.Start()
	// test existing
	if sub := af.FindActor("foo"); sub != "alpha" {
		t.Fatal("hardcoded FindActor existing actor failure:", sub)
	}
	// test non-existing
	if sub := af.FindActor("bar"); sub != "" {
		t.Fatal("hardcoded FindActor non-existing actor failure:", sub)
	}
	// shutdown
	af.Stop()
}

func Test_parseActorFile(t *testing.T) {
	strdat := `{
"foo":"alpha",
"bar":"alpha",
"baz":"beta"
}`
	actors, err := parseActorFile([]byte(strdat))
	if err != nil {
		t.Fatal("parseActorFile error:", err)
	}
	expected := map[string]string{"foo": "alpha", "bar": "alpha", "baz": "beta"}
	if !reflect.DeepEqual(actors, expected) {
		t.Fatal("parseActorFile failure:", actors)
	}
}

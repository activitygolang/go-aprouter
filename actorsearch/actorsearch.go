package actorsearch

import (
	"fmt"
	"encoding/json"
	"io/ioutil"
)

const (
	HARDCODE = iota // Using a supplied map of actor locations, meant for tests
	FILE // Load actor locations from file
	// SERVER // Request locations from other server
)

type request struct {
	Actor   string
	Retchan chan string
}

type ActorFinder struct {
	requests chan request
	shutdown chan bool
	mode int // What type of storage / lookup mode we are in
	actors map[string]string // Actor locations, if we are managing directly
	filename string // path if in file mode
}

// Setup & control
func NewActorFinder() (a *ActorFinder) {
	a = new(ActorFinder)
	a.requests = make(chan request, 8)
	a.shutdown = make(chan bool, 1)
	return a
}

func (a *ActorFinder) SetHardcodedMode(actors map[string]string) {
	a.mode = HARDCODE
	a.actors = actors
}

func (a *ActorFinder) SetFileMode(filename string) (err error) {
	actors, err := loadActorFile(filename)
	if err != nil {
		return err
	}
	a.filename = filename
	a.actors = actors
	a.mode = FILE
	return nil
}

// Loop handling

func (a *ActorFinder) Start() {
	a.Stop() // Ensure we aren't running multiple instances
	go a.mainloop()
}

func (a *ActorFinder) Stop() {
	select {
	case a.shutdown <- true: // non-blocking, via select
	default:
	}
}

func (a *ActorFinder) mainloop() {
	// Clear shutdown channel
	select {
	case <-a.shutdown:
	default:
	}
	// Run mainloop
	for {
		select {
		case req := <-a.requests:
			subserver := a.lookup(req.Actor)
			req.Retchan <- subserver
		case <-a.shutdown:
			return
		}
	}
}

func (a *ActorFinder) lookup(actorID string) (subserver string) {
	switch a.mode {
	case FILE: // File still loads everything in like a hardcode
		fallthrough
	case HARDCODE:
		subserver, ok := a.actors[actorID]
		if !ok {
			return ""
		} else {
			return subserver
		}
	default:
		return ""
	}
}

// Request API

func (a *ActorFinder) FindActor(actorID string) (subserver string) {
	fmt.Println("FindActor:", actorID)
	// TODO: running detection?
	// Assemble message
	req := request{}
	req.Actor = actorID
	req.Retchan = make(chan string, 1)
	// send message
	a.requests <- req
	// wait for result
	result := <-req.Retchan
	fmt.Println("FindActor,", actorID, ":", result)
	return result
}

// File handling

func loadActorFile(filename string) (actors map[string]string, err error) {
	// Load and parse the data
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return parseActorFile(data)
}

func parseActorFile(data []byte) (actors map[string]string, err error) {
	var tmp interface{}
	err = json.Unmarshal(data, &tmp)
	if err != nil {
		return nil, err
	}
	typedTmp := tmp.(map[string]interface{})
	// Transfer to properly typed map
	actors = make(map[string]string, len(typedTmp))
	for k, v := range typedTmp {
		switch typedV := v.(type) {
		case string:
			actors[k] = typedV
		default:
			// invalid type..... what do?
		}
	}
	return actors, nil
}

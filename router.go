package main

import (
	"os"
	"strconv"
	"fmt"
	"net/http"
	"encoding/json"
	"io/ioutil"
	"gitlab.com/activitygolang/go-aprouter/subdriver"
	"gitlab.com/activitygolang/go-aprouter/actorsearch"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Println(os.Args[0], "<config file>")
		os.Exit(1)
	}
	// load config
	cfg, err := loadConfig(os.Args[1])
	if err != nil {
		fmt.Println("config load error:", err)
		os.Exit(2)
	}
	core := NewRouterData()
	core.Config = cfg
	// start actorfinder
	core.ActorFinder = actorsearch.NewActorFinder()
	err = core.ActorFinder.SetFileMode(cfg.ActorFile)
	if err != nil {
		fmt.Println("Actor file error:", err)
		os.Exit(3)
	}
	core.ActorFinder.Start()
	// start subserver drivers
	for _, subhost := range cfg.Subservers {
		// FIXME: Port ignored and embeded in host string
		driver := subdriver.NewSubdriver(subhost, 0)
		core.Subservers[subhost] = driver
		driver.Start()
	}
	// start network
	fmt.Println("Starting network....")
	netHandle := NewNetworkHandler(core)
	err = netHandle.Run()
	fmt.Println("goodbye", err)
}

type RouterData struct {
	ActorFinder *actorsearch.ActorFinder
	Subservers map[string]*subdriver.Subdriver // index on sub-server URL
	Config config
}

func NewRouterData() (r *RouterData) {
	r = new(RouterData)
	r.Subservers = make(map[string]*subdriver.Subdriver)
	return r
}

// == Config ==

type config struct {
	Host string
	Port uint16
	// certs
	Subservers []string
	ActorFile string
}

func (c *config) GetHostString() (hostport string) {
	if c.Port != 0 {
		hostport = c.Host + ":" + strconv.Itoa(int(c.Port))
	} else {
		hostport = c.Host + ":80"
	}
	return hostport
}

func loadConfig(cfgpath string) (cfg config, err error) {
	// read in file
	rawcfg, err := ioutil.ReadFile(cfgpath)
	if err != nil {
		return config{}, err
	}
	return parseConfig(rawcfg)
}

func parseConfig(cfgData []byte) (cfg config, err error) {
	// parse text
	var data map[string]interface{}
	err = json.Unmarshal(cfgData, &data)
	if err != nil {
		return config{}, err
	}
	// distribute into config struct
	if host, ok := data["host"]; ok {
		// change to string
		switch typedHost := host.(type) {
		case string:
			cfg.Host = typedHost
		default:
		}
	}
	if port, ok := data["port"]; ok {
		// change to uint16
		switch typedPort := port.(type) {
		case int:
			cfg.Port = uint16(typedPort)
		case float64:
			cfg.Port = uint16(typedPort)
		default:
		}
	}
	if subservers, ok := data["subservers"]; ok {
		cfg.Subservers = []string{}
		// change to list of strings
		switch typedSubs := subservers.(type) {
		case []string:
			cfg.Subservers = typedSubs
		case []interface{}:
			for _, item := range typedSubs {
				switch typedItem := item.(type) {
				case string:
					cfg.Subservers = append(cfg.Subservers, typedItem)
				default:
				}
			}
		default:
		}
	}
	if actorfile, ok := data["actorfile"]; ok {
		// change to string
		switch typedAFile := actorfile.(type) {
		case string:
			cfg.ActorFile = typedAFile
		default:
		}
	}
	return cfg, nil
}

// ========== Network front end =============

type NetworkHandler struct {
	Core *RouterData // reference so we can access the rest of the system
}

func NewNetworkHandler(core *RouterData) (n *NetworkHandler) {
	n = new(NetworkHandler)
	n.Core = core
	return n
}

// network run code

func (n *NetworkHandler) Run() (err error) {
	host := n.Core.Config.GetHostString()
	err = http.ListenAndServe(host, n) // TLS addr, cert, key, handler
	return err
}

func (n *NetworkHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	// get request path
	path := req.URL.Path
	fmt.Println("ServeHTTP", path)
	// get targeted actor...... what if no target?
	subserver := n.Core.ActorFinder.FindActor(path)
	if subserver == "" {
		// TODO No subserver for this path.... just drop it?
		resp.WriteHeader(404)
		resp.Write([]byte("not found"))
		return
	}
	// send to subserver driver
	driver, ok := n.Core.Subservers[subserver]
	if !ok {
		// this subserver is unknown by the system, fail
		resp.WriteHeader(500)
		resp.Write([]byte("Internal Server Error"))
		return
	}
	subResp, err := driver.SendPacket(req)
	if err != nil {
		// FIXME: what do?
		resp.WriteHeader(500)
		resp.Write([]byte("Internal Server Error"))
		return
	}
	// put response into resp and return
	// write headers
	tgtHeaders := resp.Header()
	for key, vals := range subResp.Header {
		tgtHeaders[key] = vals
	}
	// write status
	resp.WriteHeader(subResp.StatusCode)
	// pull data
	data, err := ioutil.ReadAll(subResp.Body)
	if err != nil {
		// FIXME: what do?
	}
	// write data
	count, err := resp.Write(data)
	if err != nil || count != len(data) {
		// FIXME: what do?
	}
	return
}

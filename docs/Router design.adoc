= Router design =

This document describes the overall structure and layout of the cluster router.

It does not cover details such as specific data types or fields, but does cover the high level logic of out the system works.

== "Nodes" ==

* note: a different name could be chosen, "module" is rejected because of conflict with Go's module concept, and "node" implies a locus of control and/or data

The router is constructed out of multiple nodes which are both data structures and threads. These nodes communicate through channels.

Each node is meant to handle a single task, with a data flow of recieving an object/message, doing some processing on it (possibly calling out to another node to retrieve some data), and then sending it to whichever node is next in the processing chain.

=== Return-chans ===

At the core of the inter-node communications are "return chans". A return-chan is a small channel who's only purpose is to be the path by which a response is returned to.

If thread A needs to make a request of thread B it will call a method to handle the communication which will:

* create a return-chan
* wrap up the request message with the return-chan
* shove it into thread B's communication channel
* block on reading a response from the return-chan
* return the result

Once thread B recieves the request it will do whatever processing is required, and then puts the result in the return-chan. For simple tasks this can be done in a single thread, in more complex ones thread B can spawn a handler-thread which does the processing, uses the return-chan, and ends without disturbing thread B.

This design simplifies most of the problems inherent in multi-threaded software to to a few robust concepts which are easy for humans to mentally model. It also simplifies the code, as there is no need to worry about dealing with locks - that is handled by the language - and all thread B has to do is spin on a select statement. With a moderate amount of design smarts this setup can also eliminate the possibility of deadlocked code.

Additionally, because all mutable data is being transferred through channels and only one thread is using a piece of data at a given time the issues around memory sharing can be eliminated.

== Network layer / Mastodon interface ==

The thread structure of this node is mediated through Go's network libraries, however it cooresponds to a thread per connection

When an https request is recieved the handler will:

* check/handle authentication through oauth or other
* probably call out to the actor search interface to determine which subserver the message should be sent to
* relays the message and waits for a response
* returns the response to the network requestor

== Actor search interface ==

* Initially this will just be a big JSON file read into the router. Later it will be its own server.

The purpose of the ASI is to allow other parts of the system to easily discover where a particular actor is located in the cluster. Its core logic is a very simple recieve request, lookup, return result loop.

== User database ==

Contains the account data connecting username/password/oauth to an actor, plus any relevant metadata.

Like the ASI this is a request/lookup/return system, but with more complicated storage requirements.

== Sub-server drivers ==

These are the nodes which handle communication with the various sub-servers. Each sub-server has a driver node dedicated to it. Depending on the particular type of sub-server the driver may need to translate between different protocols; for example a sub-server which speaks ActivityPub will need Mastodon objects translated into their ActivityPub equivalents. [Whether it is better to translate here or add an additional protocol to a sub-server is an open question]

This is also one of the more complicated nodes when it comes to routing logic, as it has to handle multiple request/response pairs which are potentially multiplexed together. Depending on intra-cluster communication design each driver node may consist of multiple threads; a primary dispatch thread, with handler threads for each message pair.

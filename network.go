package main

import (
	"net/http"
)

type NetworkHandler struct {
	Core *RouterData // reference so we can access the rest of the system
}

func NewNetworkHandler(core *RouterData) (n *NetworkHandler) {
	n = new(NetworkHandler)
	n.Core = core
	return n
}

// network run code

func (n *NetworkHandler) Run() {
	http.ListenAndServeTLS(addr, cert, key, n)
}

func (n *NetworkHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	// get request path
	path := req.Url.Path
	// get targeted actor...... what if no target?
	subserver := n.Core.ActorFinder.FindActor(path)
	// send to subserver driver
	driver, ok := n.Core.Subservers[subserver]
	if !ok {
		// this actor is unknown by the system, fail
	}
	subResp, err := driver.SendPacket(req)
	// put response into resp and return
}

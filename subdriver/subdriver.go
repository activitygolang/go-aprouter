package subdriver

import (
	"fmt"
	"net/http"
)

type request struct {
	Request *http.Request
	Retchan chan result
}

type result struct {
	Response *http.Response
	Err      error
}

type Subdriver struct {
	hostname string
	port     int
	requests chan request
	shutdown chan bool
}

func NewSubdriver(hostname string, port int) (s *Subdriver) {
	s = new(Subdriver)
	s.hostname = hostname
	s.port = port
	s.requests = make(chan request, 8)
	s.shutdown = make(chan bool, 1)
	return s
}

func (s *Subdriver) Start() {
	s.Stop() // Ensure we aren't running multiple instances
	go s.mainloop()
}

func (s *Subdriver) Stop() {
	select {
	case s.shutdown <- true: // non-blocking; we may not be running
	default:
	}
}

// mainloop
func (s *Subdriver) mainloop() {
	select { // Clear out shutdown channel
	case <-s.shutdown:
	default:
	}
	// Run mainloop
	for {
		select {
		case req := <-s.requests:
			go s.sendPacket(req)
		case <-s.shutdown:
			return
		}
	}
}

func (s *Subdriver) sendPacket(req request) {
	// assemble data and internal URL
	fmt.Println("sendPacket:", req.Request)
	host := s.hostname
	if s.port != 0 {
		host = fmt.Sprintf("%s:%d", s.hostname, s.port)
	}
	req.Request.URL.Host = host
	// send request
	client := &http.Client{}
	resp, err := client.Do(req.Request)
	fmt.Println("......", resp)
	fmt.Println("err:", err)
	// send back through returnchan
	res := result{
		Response: resp,
		Err:      err,
	}
	req.Retchan <- res
	// end this thread
}

func subRequestFromRequest(subserver string, req *http.Request) (subReq *http.Request) {
	// generate new URL
	url := req.URL
	url.Host = subserver
	subURL := url.String()
	// reader.......
	subReq = http.NewRequest(req.Method, subURL)
	// pull headers
	return subReq
}

// API

func (s *Subdriver) SendPacket(req *http.Request) (resp *http.Response, err error) {
	// Assemble message
	subreq := request{}
	subreq.Request = req
	subreq.Retchan = make(chan result, 1)
	s.requests <- subreq
	// wait for result
	subResult := <-subreq.Retchan
	return subResult.Response, subResult.Err
}

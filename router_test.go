package main

import (
	"testing"
	"reflect"
)

func Test_parseConfig(t *testing.T) {
	// Test good data
	goodTestData := []byte(`{
"host": "foo",
"port": 1234,
"actorfile": "/dev/null",
"subservers": ["foo", "bar"]
}`)
	cfg, err := parseConfig(goodTestData)
	if err != nil {
		t.Fatal("parseConfig good data error:", err)
	}
	expected := config{
		Host: "foo",
		Port: 1234,
		ActorFile: "/dev/null",
		Subservers: []string{"foo", "bar"},
	}
	if !reflect.DeepEqual(cfg, expected) {
		t.Fatal("parseConfig good data failure:", cfg)
	}
	// Test bad data
	badTestData := []byte(`{
"host": 1,
"port": 1234890987490803,
"actorfile": null,
"subservers": [5, null]
}`)
	cfg, err = parseConfig(badTestData)
	if err != nil {
		t.Fatal("parseConfig bad data error:", err)
	}
	expected = config{
		Host: "",
		Port: 0,
		ActorFile: "",
		Subservers: []string{},
	}
	if !reflect.DeepEqual(cfg, expected) {
		t.Fatal("parseConfig bad data failure:", cfg)
	}
}
